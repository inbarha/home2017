<?php

use yii\db\Migration;

/**
 * Handles the creation of table `activity`.
 */
class m170716_082847_create_activity_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('activity', [
            'id' => $this->primaryKey(),
			'tatle' => $this->string()->notNull(),
			'categoryId' => $this->integer()->notNull(),
			'statusId' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('activity');
    }
}
