<?php

use yii\db\Migration;

/**
 * Handles adding Categoryld to table `user`.
 */
class m170716_080854_add_Categoryld_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'Categoryld', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'Categoryld');
    }
}
