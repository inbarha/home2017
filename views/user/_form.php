<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\user;
use app\models\Category;
/*<?= $form->field($model, 'role')->dropDownList(User::getRoles(),['disabled' => !\Yii::$app->user->can('changeOwner')]); ?>


/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'auth_key')->textInput(['maxlength' => true]) ?>
	

		

	<?php if(!$model->isNewRecord){ ?>
		<?= $form->field($model, 'CategoryId')->dropDownList(Category::getCategory()) ?>
		<?= $form->field($model, 'role')->dropDownList(User::getRoles()) ?>
	<?php } ?>

	
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
