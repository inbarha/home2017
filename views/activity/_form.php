<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Status;
use app\models\Category;



/* @var $this yii\web\View */
/* @var $model app\models\Activity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activity-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tatle')->textInput(['maxlength' => true]) ?>
	
<?= $form->field($model, 'categoryId')->dropDownList(Category::getCategory(),['disabled' => !\Yii::$app->user->can('changeCategory')]); ?>  
	
	<?php if(!$model->isNewRecord){ ?>
	 	<?= $form->field($model, 'statusId')->dropDownList(Status::getStatuses()) ?>
	<?php } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
