<?php
namespace app\rbac;

use yii\rbac\Rule;
use Yii; 

class OwnActivityRule extends Rule
{
	public $name = 'ownActivityRule';

	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			return isset($params['activity']) ? $params['activity']->categoryId == $user : false;
		}
		return false;
	}
}



