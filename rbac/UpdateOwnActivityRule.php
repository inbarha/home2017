<?php 

	namespace app\rbac;

	use yii\rbac\Rule;
	use app\models\User;
	use app\models\Activity;
	use yii\web\NotFoundHttpException;

	/**
	 * Checks if authorID matches user passed via params
	 */
	class UpdateOwnActivityRule extends Rule
	{
		public $name = 'UpdateOwnActivity';

		/**
		 * @param string|int $user the user ID.
		 * @param Item $item the role or permission that this rule is associated with
		 * @param array $params parameters passed to ManagerInterface::checkAccess().
		 * @return bool a value indicating whether the rule permits the role or permission it is associated with.
		 */
		public function execute($user, $item, $params)
		{	
			if(isset($_GET['id'])){
				$userCategory = User::findOne($user);
				$activityCategory = Activity::findOne($_GET['id']);

				if(isset($userCategory) && isset($activityCategory)){
					if($userCategory->CategoryId == $activityCategory->categoryId)
						return true;
				}
			}
		
			return false;
		}
	}
	
?>