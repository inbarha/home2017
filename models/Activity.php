<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "activity".
 *
 * @property integer $id
 * @property string $tatle
 * @property integer $categoryId
 * @property integer $statusId
 */
class Activity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tatle',], 'required'],
            [['categoryId', 'statusId'], 'integer'],
            [['tatle'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tatle' => 'Tatle',
            'categoryId' => 'Category ID',
            'statusId' => 'Status ID',
        ];
    }
	
	  public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);
		
        if ($this->isNewRecord)
		    $this->statusId = '2';
		
		if ($this->isAttributeChanged('categoryId')){
			if(!\Yii::$app->user->can('changeCategory')){
				unset($this->categoryId);
			}
		}

        return $return;
    }
	
		public function getcategoryItem()
    {
        return $this->hasOne(Category::className(), ['id' => 'categoryId']);
    }	
	
	
}

